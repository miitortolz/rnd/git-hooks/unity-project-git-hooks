# Unity Git Hooks (derived from [doitian/unity-git-hooks](https://github.com/doitian/unity-git-hooks))

Git hooks for Unity project.

To manage a Unity project, the Assets meta file should also been added to
repository. But sometimes the meta files and the corresponding asset files
and directories are inconsistent, then meta files will lead to conflicts when
a team is collaborating on the same code base.

## Features

- Stop committing if meta files and asset files and directories are
  inconsistent. If an asset file is added, its meta file and meta files of all
  its containing directories should also been added. If a asset file is
  deleted, its meta file and meta files of all its empty containing
  directories should also been deleted. When meta files are added/deleted,
  asset files and directories should also been consistent.
- Delete empty asset directories after checkout and merge. Unity keep
  generating meta file for empty asset directory, but git does not trace
  directory.

## Installation

1. Windows 
   - Manual
     - Double click the `install-hooks.bat` file.
   - Command line
     - Open `cmd` and go to the unity root directory.
     - Run the batch script `install-hooks.bat`.
1. Mac
   - Open `Terminal` and go to the unity root directory.
   - Run the shell script `shell install-hooks.sh`.
1. Cross Platform with Python
   - Python Install `stable release` of [python-3.7.4](https://www.python.org/downloads/windows/).
   - Open `cmd` or `terminal` and go to the unity root directory.
   - Run the python script `python install-hooks.py`.

## General Info

All of our hook scripts (`post-checkout` `post-merge` and `pre-commit`) are in `.githooks` folder and the commands mentioned in the `Installation` will set the `.githooks` as path in `core.hookspath` of `git config`.

It is assumed that Assets directory is located in the root directory of the
repository. It can be configured using git config. Following example tells the
scripts that the assets directory is `Assets`.

## Git Commands

```
git config core.hookspath .githooks
git config unity3d.assets-dir Assets
```