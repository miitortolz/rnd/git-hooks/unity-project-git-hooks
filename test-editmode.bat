@ECHO OFF

SET PLATFORM=editmode
SET OUTPUT_PATH=Tests/test-editmode.xml

:CHECK_UNITY
REM Check for Environment Variable UNITY.
IF "%UNITY%" == "" GOTO INPUT_UNITY
GOTO UNITYTEST

:INPUT_UNITY
REM Get path to Unity.exe from input.
SET /P UNITY=Enter path to Unity.exe: 
GOTO UNITYTEST

:UNITYTEST
REM Run tests.
ECHO Running tests. (%UNITY%) (%PLATFORM%) (%OUTPUT_PATH%)
"%UNITY%" -batchmode -nographics -runTests -testPlatform %PLATFORM% -testResults "%OUTPUT_PATH%"
ECHO.
GOTO END

:END
ECHO.