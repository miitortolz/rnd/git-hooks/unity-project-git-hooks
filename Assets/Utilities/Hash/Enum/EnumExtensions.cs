#region File Header
// Filename: EnumExtensions.cs
// Author: Aries Sanchez Sulit
// Date Created: 2019/08/14
// Copyright (c) 2019 Fortend
#endregion

namespace Utilities.Hash.Enum
{
    // Alias
    using Enum = System.Enum;
    
    /// <summary>
    /// Hash generator of two combined enums
    /// </summary>
    public static class EnumExtensions
    {
        private const int OFFSET = 42;
        private const int STEP = 23;
        
        /// <summary>
        /// Returns a hash of the combined enum
        /// </summary>
        /// <param name="t1">Enum value 1</param>
        /// <param name="t2">Enum value 2</param>
        /// <typeparam name="T1">Enum Type 1</typeparam>
        /// <typeparam name="T2">Enum Type 2</typeparam>
        /// <returns>hash</returns>
        public static int ToHash<T1, T2>(T1 t1, T2 t2) 
        //        where T1 : Enum
        //        where T2 : Enum
        {
            var i = (int)(object)t1;
            var j = (int)(object)t2;
            
            unchecked
            {
                return (OFFSET * STEP + i) * STEP + j;
            }
        }

        /// <summary>
        /// An Enum extension method for Hashing different type of enum
        /// </summary>
        /// <param name="t1">Enum value 1</param>
        /// <param name="t2">Enum value 2</param>
        /// <typeparam name="T">Enum Type 2</typeparam>
        /// <returns>hash</returns>
        public static int ToHash<T>(this Enum t1, T t2) 
        //        where T : Enum
        {
            var i = (int)(object)t1;
            var j = (int)(object)t2;
            
            unchecked
            {
                return (OFFSET * STEP + i) * STEP + j;
            }
        }
    }
}