#region File Header
// Filename: UnityVersion.cs
// Author: Aries Sanchez Sulit
// Date Created: 2019/08/15
// Copyright (c) 2019 Fortend
#endregion

using UnityEngine;
using UnityEngine.UI;

namespace Runtime
{
    public class UnityVersion : MonoBehaviour
    {
        #region Fields

        [SerializeField] private Text _unityVersion;

        #endregion Fields

        #region Unity Life Cycle

        private void Awake()
        {
            _unityVersion.text = Application.unityVersion;
        }

        #endregion Unity Life Cycle
    }
}