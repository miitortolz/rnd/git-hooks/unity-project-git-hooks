#region File Header
// Filename: EnumHashTests.cs
// Author: Aries Sanchez Sulit
// Date Created: 2019/08/14
// Copyright (c) 2019 Fortend
#endregion

using System.Collections.Generic;
using NUnit.Framework;
using Utilities.Hash.Enum;
using EnumHash = Utilities.Hash.Enum.EnumExtensions;

namespace Utilities.Tests.Enum
{
    #region Enums

    public enum Body
    {
        Head,
        Body
    }

    public enum Weapon
    {
        Dagger,
        Sword
    }

    #endregion Enums
    
    public class EnumHashTests
    {
        #region Fields

        private HashSet<int> _hash;

        #endregion Fields

        #region Setup

        [SetUp]
        public void Setup()
        {
            _hash = new HashSet<int>();
        }

        [TearDown]
        public void TearDown()
        {       
            _hash.Clear();
            _hash = null;
        }
        
        #endregion Setup

        #region Tests

        [Test]
        public void _Assert_If_Different_Set_Of_Enums_Returns_The_Same_Hash()
        {
            _hash.Add(EnumHash.ToHash(Body.Head, Weapon.Dagger));
            _hash.Add(EnumHash.ToHash(Body.Head, Weapon.Sword));
            _hash.Add(EnumHash.ToHash(Body.Body, Weapon.Dagger));
            _hash.Add(EnumHash.ToHash(Body.Body, Weapon.Sword));
            
            Assert.True(_hash.Count == 4);
        }
        
        // ReSharper disable HeapView.BoxingAllocation
        [Test]
        public void _Assert_If_Different_Set_Of_Enums_Returns_The_Same_Hash_Using_Ext_Method()
        {
            _hash.Add(Body.Head.ToHash(Weapon.Dagger));
            _hash.Add(Body.Head.ToHash(Weapon.Sword));
            _hash.Add(Body.Body.ToHash(Weapon.Dagger));
            _hash.Add(Body.Body.ToHash(Weapon.Sword));
            
            Assert.True(_hash.Count == 4);
        }
        // ReSharper enable HeapView.BoxingAllocation
        
        #endregion Tests
    }
}